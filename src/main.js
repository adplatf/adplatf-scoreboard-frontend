import Toasted from 'vue-toasted';
import Axios from 'axios';
import Vue from 'vue';
import App from './App.vue';

import store from './store';
import 'fontsource-open-sans';
import 'fontsource-open-sans/600.css';
import 'fontsource-jetbrains-mono';
import 'normalize.css/normalize.css';

import {VClosePopover, VPopover, VTooltip,} from 'v-tooltip';

VTooltip.options.disposeTimeout = 200;
VTooltip.options.defaultHtml = false;

Vue.directive('tooltip', VTooltip);
Vue.directive('close-popover', VClosePopover);
Vue.component('v-popover', VPopover);

Vue.use(Toasted, {
	duration: 8000,
	keepOnHover: true,
	className: 'toasted-toast',
});

const axiosInstance = Axios.create({
	baseURL: '/',
	headers: {
		'Accept': 'application/json',
		'Content-Type': 'application/json',
	},
	timeout: 60000,
});

// noinspection JSUnusedGlobalSymbols
Vue.prototype.$http = axiosInstance;

Vue.config.productionTip = false;

new Vue({
	store,
	render: h => h(App),
}).$mount('#app');
