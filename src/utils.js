export function errorToToast(e) {
	if (!e?.response?.data) {
		return e.toString();
	}
	if (e?.response?.status === 500) {
		return e.response.statusText;
	}
	return e.response.data;
}
