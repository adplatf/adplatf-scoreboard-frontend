import Vue from 'vue';
import Vuex from 'vuex';

Vue.use(Vuex);

// noinspection JSCheckFunctionSignatures
export default new Vuex.Store({
	state: {
		wsConnected: false,

		// From GET /state
		config: {},
		currentLeader: -1,
		firstBloods: {},
		scores: {},
		state: {},
		statuses: {},
	},
	mutations: {
		setWsConnected: (state, n) => state.wsConnected = n,

		setGameState: (state, n) => {
			state.config = n.config;
			state.currentLeader = n.currentLeader;
			state.firstBloods = n.firstBloods;
			state.scores = n.scores;
			state.state = n.state;
			state.statuses = n.statuses;
		},

		// From WS
		processFlagStolenEvent: (state, n) => {
			const thiefId = n.thiefTeam.toString();
			const victimId = n.victimTeam.toString();
			const service = n.service.toString();
			Vue.set(state.scores[thiefId][service], 'stolenFlags', state.scores[thiefId][service].stolenFlags + 1);
			Vue.set(state.scores[victimId][service], 'lostFlags', state.scores[victimId][service].lostFlags + 1);
		},
		processUpdateStatusEvent: (state, n) => {
			const teamId = n.team.toString();
			const serviceId = n.service.toString();

			Vue.set(state.statuses[teamId], serviceId, {
				status: n.status,
				description: n.description,
				sla: n.sla,
			});
		},
		processUpdateScoreEvent: (state, n) => {
			const teamId = n.team.toString();
			const serviceId = n.service.toString();

			Vue.set(state.scores[teamId][serviceId], 'score', n.newScore);
		},
		processFirstBloodEvent: (state, n) => {
			const ServiceId = n.service.toString();
			Vue.set(state.firstBloods, ServiceId, n.team);
		},
		processGameStartEvent: (state) => {
			Vue.set(state.state, 'gameStarted', true);
		},
		processNewRoundEvent: (state, n) => {
			Vue.set(state.state, 'currentRound', n.roundNum);
			Vue.set(state.state, 'currentRoundStart', n.roundStart);
		},
		processGameEndEvent: (state) => {
			Vue.set(state.state, 'gameEnded', true);
		},
		processFreezeStartEvent: (state) => {
			Vue.set(state.state, 'freeze', true);
		},
	},
});
